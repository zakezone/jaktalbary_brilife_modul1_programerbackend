﻿namespace EntryDataPemakaiAlatKontrasepsi
{
    partial class EntryData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgventrypemakaikontrasepsi = new System.Windows.Forms.DataGridView();
            this.labelpropinsi = new System.Windows.Forms.Label();
            this.labelkontrasepsi = new System.Windows.Forms.Label();
            this.labeljumlahpemakaian = new System.Windows.Forms.Label();
            this.txtpropinsi = new System.Windows.Forms.TextBox();
            this.txtkontrasepsi = new System.Windows.Forms.TextBox();
            this.txtjumlahpemakai = new System.Windows.Forms.TextBox();
            this.btninsert = new System.Windows.Forms.Button();
            this.btnupdate = new System.Windows.Forms.Button();
            this.btndelete = new System.Windows.Forms.Button();
            this.labelJudul = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgventrypemakaikontrasepsi)).BeginInit();
            this.SuspendLayout();
            // 
            // dgventrypemakaikontrasepsi
            // 
            this.dgventrypemakaikontrasepsi.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgventrypemakaikontrasepsi.Location = new System.Drawing.Point(10, 145);
            this.dgventrypemakaikontrasepsi.Name = "dgventrypemakaikontrasepsi";
            this.dgventrypemakaikontrasepsi.Size = new System.Drawing.Size(603, 348);
            this.dgventrypemakaikontrasepsi.TabIndex = 0;
            // 
            // labelpropinsi
            // 
            this.labelpropinsi.AutoSize = true;
            this.labelpropinsi.Location = new System.Drawing.Point(12, 51);
            this.labelpropinsi.Name = "labelpropinsi";
            this.labelpropinsi.Size = new System.Drawing.Size(72, 13);
            this.labelpropinsi.TabIndex = 1;
            this.labelpropinsi.Text = "Kode Propinsi";
            // 
            // labelkontrasepsi
            // 
            this.labelkontrasepsi.AutoSize = true;
            this.labelkontrasepsi.Location = new System.Drawing.Point(12, 76);
            this.labelkontrasepsi.Name = "labelkontrasepsi";
            this.labelkontrasepsi.Size = new System.Drawing.Size(90, 13);
            this.labelkontrasepsi.TabIndex = 1;
            this.labelkontrasepsi.Text = "Kode Kontrasepsi";
            // 
            // labeljumlahpemakaian
            // 
            this.labeljumlahpemakaian.AutoSize = true;
            this.labeljumlahpemakaian.Location = new System.Drawing.Point(12, 101);
            this.labeljumlahpemakaian.Name = "labeljumlahpemakaian";
            this.labeljumlahpemakaian.Size = new System.Drawing.Size(84, 13);
            this.labeljumlahpemakaian.TabIndex = 1;
            this.labeljumlahpemakaian.Text = "Jumlah Pemakai";
            // 
            // txtpropinsi
            // 
            this.txtpropinsi.Location = new System.Drawing.Point(136, 48);
            this.txtpropinsi.Name = "txtpropinsi";
            this.txtpropinsi.Size = new System.Drawing.Size(100, 20);
            this.txtpropinsi.TabIndex = 2;
            // 